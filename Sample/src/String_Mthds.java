
public class String_Mthds {
	String s1 = "my name is kartheek", s2 = "intial is Segu";

	void strCmp() {
		System.out.println(s1.compareTo(s2));
	}

	void strContains() {
		System.out.println("Method over laodded without parameters");
		System.out.println(s1.contains("karth"));

	}

	void strContains(String s) {
		System.out.println("Method over loading with parameters");
		System.out.println(s1.contains(s));
		System.out.println("Method over loading with parameters");
	}

	void strLength() {
		System.out.println("The length of string s1 is : " + s1.length());
		System.out.println("The length of string s2 is : " + s2.length());
	}

	void strChar() {
		System.out.println("The length of s1 is " + s1.charAt(8));
	}

	void strCaseChange() {
		System.out.println("Changing the cases from lower to upper" + s1.toUpperCase());
		System.out.println("Changing the cases from lower to upper" + s2.toUpperCase());
	}

	void strGetChar() {
		System.out.println("the cahrecter in 7th place in s2 string is" + s2.charAt(10));
	}

	void strCharIndex() {
		System.out.println("the index of charecter 7th is " + s2.indexOf(4));
	}
}
